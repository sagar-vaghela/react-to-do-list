import React from "react";
import PropTypes from "prop-types";

const AlertMsg = ({ error, success }) => {
  const alertClass = success ? "alert-success" : "alert-danger";
  const msg = error ? error : success ? success : "";
  return (
    <div className={"alert " + alertClass} role="alert">
      {msg}
    </div>
  );
};

AlertMsg.propTypes = {
  error: PropTypes.any,
  success: PropTypes.any
};

export default AlertMsg;
