import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import "./css/Breadcrumb.css";

const Breadcrumb = props => {
  const paths = props.path;

  return (
    <ol className="breadcrumb">
      {/* <li className="breadcrumb-item"><Link to="/"  >Home</Link></li>*/}
      {paths.map((path, key) => {
        const active = key + 1 === paths.length ? "active" : false;
        const pathLink = active ? path : <Link to={"/"}>{path}</Link>;
        return (
          // eslint-disable-next-line react/no-array-index-key
          <li key={"path" + key} className={"breadcrumb-item " + active}>
            {pathLink}
          </li>
        );
      })}
    </ol>
  );
};

Breadcrumb.propTypes = {
  path: PropTypes.any
};

export default Breadcrumb;
