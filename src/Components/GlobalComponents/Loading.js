import React from "react";
import PropTypes from "prop-types";

// Style
import "./css/Loading.css";

const Loading = props => {
  const alert = props.error
    ? "alert-danger"
    : props.success
    ? "alert-success"
    : false;
  const message = alert ? (props.error ? props.error : props.success) : false;

  return (
    <div className="loading">
      <div className="icon">
        <div className="img">
          {alert ? (
            <div
              className={"alert " + alert}
              role="alert"
              style={{ width: "50%", margin: "auto" }}
            >
              {message}
            </div>
          ) : (
            <img src="./images/loading.svg" alt={"loading"} />
          )}
        </div>
      </div>
    </div>
  );
};

Loading.propTypes = {
  error: PropTypes.any,
  success: PropTypes.any
};

export default Loading;
