/*
 * PageHeader: Compnent, display on the top of page content
 */

/********************
 *   Import Packages
 *********************/

// Main Packages
import React from "react"; // React & React component
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

// Material UI
import FloatingActionButton from "material-ui/FloatingActionButton";
import ContentAdd from "material-ui/svg-icons/content/add";

// Style
import "./css/PageHeader.css";

const PageHeader = ({ icon, addbtn, title }) => {
  return (
    <div className="page-header">
      <h2>
        {icon}
        {title}

        {addbtn && (
          <Link to={addbtn}>
            <FloatingActionButton mini className="addBtn">
              <ContentAdd />
            </FloatingActionButton>
          </Link>
        )}
      </h2>
    </div>
  );
};

PageHeader.propTypes = {
  icon: PropTypes.any,
  title: PropTypes.any,
  addbtn: PropTypes.any
};

export default PageHeader;
