import React from "react";
import PropTypes from "prop-types";

// Style
import "./css/PagesContainer.css";

const PagesContainer = ({ children }) => {
  return (
    <div className="col-lg-12 col-md-12 pt-3 page-content">{children}</div>
  );
};

PagesContainer.propTypes = {
  children: PropTypes.any
};

export default PagesContainer;
