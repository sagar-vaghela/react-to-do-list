import React from "react";
import PropTypes from "prop-types";

// Style
import "./css/Pagination.css";

function paginationNumbers(total, show, current) {
  let limit = 7;
  const pages = Math.ceil(total / show, 1);
  limit = pages <= limit ? pages : limit;
  const range = Math.floor(limit / 2, 1);
  let end = current > range ? current + range : limit;
  end = end >= pages ? pages : end;

  let start = current > range ? current - range : 1;
  start = end === pages ? end - limit + 1 : start;

  const result = [];

  for (; start <= end; start++) {
    result.push(start);
  }

  return result;
}

const Pagination = props => {
  const { currentPage, onPageChange, total, show } = props;
  const pages = paginationNumbers(total, show, parseInt(currentPage));
  const pagesCount = Math.ceil(total / show, 1);

  if (pages.length <= 1) return false;

  return (
    <nav aria-label="..." className="pagination-container">
      <ul className="pagination">
        <li className={"page-item " + (currentPage === 1 ? "disabled" : "")}>
          <a
            className="page-link"
            href="#/"
            tabIndex="-1"
            onClick={e => onPageChange(e, currentPage - 1)}
          >
            Previous
          </a>
        </li>

        {pages.map(page => {
          const active = page === currentPage ? "active" : "";
          return (
            <li className={"page-item " + active} key={"pagin" + page}>
              <a
                className="page-link"
                href="#/"
                onClick={e => onPageChange(e, page)}
              >
                {page}
              </a>
            </li>
          );
        })}

        <li
          className={
            "page-item " + (currentPage === pagesCount ? "disabled" : "")
          }
        >
          {
            // eslint-disable-next-line jsx-a11y/anchor-is-valid
            <a
              className="page-link"
              href="#"
              onClick={e => onPageChange(e, parseInt(currentPage) + 1)}
            >
              Next
            </a>
          }
        </li>
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  currentPage: PropTypes.any,
  onPageChange: PropTypes.func,
  total: PropTypes.any,
  show: PropTypes.any
};

export default Pagination;
