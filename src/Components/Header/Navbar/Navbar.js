import React from "react";

import UserProfileBtn from "./userProfileBtn/UserProfileBtn";
import "./navStyle.css";

const Navbar = () => {
  const isLogged = !!sessionStorage.getItem("TOKEN");
  return (
    <nav
      className={
        isLogged?
        "navbar navbar-toggleable-md navbar-light bg-faded fixed-top"
        : ""
      }
    >
      {isLogged && <UserProfileBtn />}
    </nav>
  );
};

export default Navbar;
