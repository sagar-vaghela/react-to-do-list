/*
 * App: the main component
 * Child from Navbar
 */

/********************
 *   Import Packages
 *********************/

// Main Packages
import React, { Component } from "react"; // React & React component

// Material UI
import FlatButton from "material-ui/FlatButton";
import Popover from "material-ui/Popover";
import MenuItem from "material-ui/MenuItem";
import KeyboardReturn from "material-ui/svg-icons/hardware/keyboard-return";

// Style
import "./userProfileStyle.css";

class UserProfileBtn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false // for open and close the menu
    };
  }

  // handle Open Menu
  handleOnClick = event => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  };

  // handle Close Menu
  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  handleLogout = e => {
    e.preventDefault();
    sessionStorage.removeItem("TOKEN");
    window.location.reload();
  };

  //render
  render() {
    return (
      <FlatButton
        className="user-profile-btn"
        style={{ height: "50px", padding: "3px 10px", margin: "0 0 0 auto" }}
        onClick={this.handleOnClick}
      >
        <span className="nav-username">{sessionStorage.getItem("EMAIL")}</span>

        {/*User Profile Menu*/}
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          targetOrigin={{ horizontal: "right", vertical: "top" }}
          onRequestClose={this.handleRequestClose}
          style={{ width: "200px" }}
        >
          {/* Menu List */}
          <MenuItem
            primaryText="Logout"
            leftIcon={<KeyboardReturn />}
            onClick={this.handleLogout}
          />
          {/* /End Menu List */}
        </Popover>
        {/* /End User Profile Menu*/}
      </FlatButton>
    );
  }
}

export default UserProfileBtn;
