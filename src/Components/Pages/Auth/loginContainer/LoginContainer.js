import React from "react";
import PropTypes from "prop-types";

import Navbar from "../../../Header/Navbar/Navbar";

// style
import "./loginContainerStyle.css";

const LoginContainer = ({ title, children }) => {
  return (
    <div>
      <Navbar />
      <div className="container">
        <div className="row justify-content-center login-container">
          <div className="col-lg-6 col-md-6 clearfix login-box">
            <h2>{title}</h2>
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

LoginContainer.propTypes = {
  title: PropTypes.any,
  children: PropTypes.any
};

export default LoginContainer;
