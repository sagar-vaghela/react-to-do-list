import React from "react";
import PropTypes from "prop-types";

import Navbar from "../../../Header/Navbar/Navbar";

// style
import "./registrationContainerStyle.css";

const RegistrationContainer = ({ title, children }) => {
  return (
    <div>
      <Navbar />
      <div className="container">
        <div className="row justify-content-center registration-container">
          <div className="col-lg-6 col-md-6 clearfix registration-box">
            <h2>{title}</h2>
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};

RegistrationContainer.propTypes = {
  title: PropTypes.any,
  children: PropTypes.any
};

export default RegistrationContainer;
