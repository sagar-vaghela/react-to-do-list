import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import swal from "sweetalert";
import PropTypes from "prop-types";

// ToDos Redux Actions
import { getToDo, editToDo } from "../../../../Redux/actions/todosActions";

// Global Components
import {
  PagesContainer,
  Breadcrumb,
  PageHeader,
  Loading,
  AlertMsg
} from "../../../GlobalComponents/GlobalComponents";

import EditToDoForm from "./EditToDoForm";

// Material UI Icons
import LoyaltyIcon from "material-ui/svg-icons/action/loyalty";

class EditToDo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      redirect: false
    };
  }

  componentDidMount() {
    const { dispatch, match } = this.props;
    const todoid = match.params.id;
    dispatch(getToDo(todoid)).then(() => this.setState({ loading: false }));
  }

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, match } = this.props;
    const todoid = match.params.id;
    const formData = new FormData(e.target);
    const obj = e.target;
    dispatch(editToDo(todoid, formData)).then(() => {
      const _this = this;
      obj.reset();
      swal(
        {
          title: "ToDo updated successfully",
          type: "success",
          showCancelButton: true,
          confirmButtonColor: "rgb(56, 143, 233)",
          confirmButtonText: "Back to todos list",
          closeOnConfirm: true
        },
        function() {
          _this.setState({ redirect: true });
        }
      );
    });
  };

  // render
  render() {
    // Store props
    const { todo, errMsg, fetching } = this.props;
    const { redirect } = this.state;

    // Pageheader Options
    const pageHeaderOptions = {
      title: "Edit ToDo",
      icon: <LoyaltyIcon className="pagetitle-icon" />
    };

    return (
      <PagesContainer path={this.props.location}>
        <Breadcrumb path={["todos", "edit"]} />
        <PageHeader {...pageHeaderOptions} />

        {redirect && <Redirect to="/" />}

        <div className="page-container">
          {fetching && <Loading />}

          <div className="row justify-content-center">
            <div className="col-lg-8 col-md-8">
              {todo && !this.state.loading && (
                <EditToDoForm handleSubmit={this.handleSubmit} todo={todo} />
              )}
              {errMsg && <AlertMsg error={errMsg} />}
            </div>
          </div>
        </div>
      </PagesContainer>
    );
  }
}

const mapStateToProps = store => {
  return {
    todo: store.todos.todo,
    errMsg: store.todos.errMsg,
    succMsg: store.todos.succMsg,
    fetching: store.todos.fetching
  };
};

EditToDo.propTypes = {
  dispatch: PropTypes.func,
  match: PropTypes.any,
  succMsg: PropTypes.any,
  errMsg: PropTypes.any,
  todo: PropTypes.any,
  fetching: PropTypes.any,
  location: PropTypes.any
};

export default connect(mapStateToProps)(EditToDo);
