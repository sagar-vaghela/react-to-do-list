// Main Packages
import React from "react";

// Style
import "./rowToDo.css";

export function DummyRowToDo() {
  const result = [];
  for (let i = 0; i < 5; i++) {
    result.push(
      <tr key={"dummy" + i}>
        <td>
          <span
            style={{
              background: "#ccc",
              display: "inline-block",
              width: "70%",
              height: "10px",
              borderRadius: "5px"
            }}
          />
        </td>

        <td>
          <span
            style={{
              background: "#ccc",
              display: "inline-block",
              width: "70%",
              height: "10px",
              borderRadius: "5px"
            }}
          />
        </td>

        <td>
          <span
            style={{
              background: "#ccc",
              display: "inline-block",
              width: "70%",
              height: "10px",
              borderRadius: "5px"
            }}
          />
        </td>

        <td>
          <span
            style={{
              background: "#ccc",
              display: "inline-block",
              width: "70%",
              height: "10px",
              borderRadius: "5px"
            }}
          />
        </td>

        <td>
          <span
            style={{
              background: "#ccc",
              display: "inline-block",
              width: "70%",
              height: "10px",
              borderRadius: "5px"
            }}
          />
        </td>
      </tr>
    );
  }

  return result;
}
