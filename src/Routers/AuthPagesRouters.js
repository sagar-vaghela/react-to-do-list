import React from "react";

// Authenticated Pages Routers
import ToDosRouters from "./pagesRouters/ToDosRouters";

const AuthPagesRouters = () => {
  return (
    <div className="row clearfix">
      <ToDosRouters />
    </div>
  );
};

export default AuthPagesRouters;
